## Built With

* [React](https://reactjs.org/docs/getting-started.html) - A JavaScript library for building user interfaces
* [NodeJS](https://nodejs.org/en/docs/) - Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine.
* [Socket.io](https://socket.io/docs/) - Socket.IO is a library that enables real-time, bidirectional and event-based communication between the browser and the server.

## Lancer React

* Ouvrir un terminal 
* Se placer sur le dossier 
* lancer npm install puis npm start

## Lancer le serveur node

* Ouvrir un terminal 
* Se placer sur le dossier 
* lancer node .\server.js